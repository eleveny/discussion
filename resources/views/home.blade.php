@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! <br><br>
                    <a style="text-decoration:none;font-size:18px;padding: 2px;color: black;background-color: #DCDCDC;text-align: center;cursor: pointer;width: 30%;font-size: 16px;" href="{{ url('discussion/') }}"{!! Form::button('View Discussions' , ['id'=>'button']) !!}</a>{{--This button redirects the user to the page to view discussions--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
