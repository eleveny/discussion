<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 200vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .card {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                max-width: 700px;
                margin: auto;
                text-align: center;
                font-family: arial;
                padding: 5px 20px;

            }
            .card button {
                border: none;
                outline: 0;
                padding: 12px;
                color: black;
                background-color: #DCDCDC;
                text-align: center;
                cursor: pointer;
                width: 100%;
                font-size: 18px;
            }

            .card button:hover {
              opacity: 0.7;
            }

            #button {
                border: none;
                outline: 0;
                padding: 12px;
                color: black;
                background-color: #DCDCDC;
                text-align: center;
                cursor: pointer;
                width: 100%;
                font-size: 18px;
            }

            .liness{
                background-color: black;
                height: 2px;
                width: 19px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            @auth
                <div class="title m-b-md">
                    Welcome to the Forum {{ Auth::user()->name }}
                </div>
            @else
                <div class="title m-b-md">
                    Welcome to the Forum
                </div>
            @endauth

<div  class="card">{{--Discussions are placed in a card--}}
    <ul>
    @foreach ($discussions as $discussion)
    <div>
        <h4><b>{{ $discussion->title }}</b></h4>
        <p>Description: {{ $discussion->description }}</p>
        <p>Post: {{ $discussion->post }}</p>
        @auth
            <p>Created by: {{ Auth::user()->name }} at {{ $discussion->created_at }} </p>
        @else
            <p>Created at: {{ $discussion->created_at }} </p>
        @endauth
        @auth
                {!! Form::open(['method' => 'DELETE', 'url' => 'discussion/' . $discussion->id]) !!}
                {!! Form::button('Delete', ['type' => 'submit']) !!}<br><br>
            <a href="{{ url('discussion/' . $discussion->id . '/edit') }}"{!! Form::button('Edit Discussion', ['id'=>'button']) !!}</a> {{--This button redirects the user to the page to edit the resource--}}
            <a href="{{ url('discussion/' . $discussion->id) }}"{!! Form::button('View Discussion', ['id'=>'button']) !!}</a> {{--This button redirects the user to the page to view the resource--}}
        @endauth
        {!! Form::close() !!}
<div class ="liness">
</div>
    </div>
    @endforeach
</div>
@auth {{--Authentication starts: only logged in users will see what is enclosed in auth--}}
      <br><br><a style="text-decoration:none;font-size:30px;" href="{{ url('discussion/create') }}"{!! Form::button('Add New Discussion Entry' , ['id'=>'button']) !!}</a> {{--This button redirects the user to the page to create a new resource--}}
@endauth {{--Aunthentication ends--}}
</ul>			

                
            </div>
        </div>
    </body>
</html>
