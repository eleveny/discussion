<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #button {
                border: none;
                outline: 0;
                padding: 12px;
                color: black;
                background-color: #DCDCDC;
                text-align: center;
                cursor: pointer;
                width: 50%;
                font-size: 18px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    New Discussion
                </div>
@if($errors->any())
	@foreach($errors->all() as $error)
		{{ $error }}
	@endforeach
@endif
	{!! Form::open(array('url' => '/discussion')) !!}

{{--@include('discussion.partials/discussion')--}}
<p>
	{!! Form::label('title', 'Title:')!!}
	{!! Form::text('title', $discussion->title,[
		'placeholder' =>'Enter the title'] ) !!}
</p>

<p>
    {!! Form::label('description', 'Description:')!!}
    {!! Form::text('description', $discussion->description,[
        'placeholder' =>'Enter the Description'] ) !!}
</p>

<p>	
	{!! Form::label('post', 'Post:') !!}
	{!! Form::textarea('post', $discussion->post, [
		'placeholder' =>'Enter the post'] ) !!}
</p>

	{!! Form::submit('Post to Forum', ['id'=>'button']) !!}<br><br>

    <a style="text-decoration:none;font-size:18px;" href="{{ url('discussion/') }}"{!! Form::button('View Discussions' , ['id'=>'button']) !!}</a>{{--This button redirects the user to the page to view discussions--}}

	{!! Form::close() !!}

               
            </div>
        </div>
    </body>
</html>
