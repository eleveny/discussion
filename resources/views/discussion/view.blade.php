<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #button {
                border: none;
                outline: 0;
                padding: 12px;
                color: black;
                background-color: #DCDCDC;
                text-align: center;
                cursor: pointer;
                width: 50%;
                font-size: 18px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Display Discussion Entry
                </div>
<table>
<thead>
<tr>
	<th>Field</th>
	<th>Value</th>
</tr>
</thead>

<tbody>
<tr>
	<td>Title</td>
	<td>{{$discussion->title}}</td>
</tr>
<tr>
	<td>Description</td>
	<td>{{$discussion->description}}</td>
</tr>
<tr>
    <td>Post</td>
    <td>{{$discussion->post}}</td>
</tr>
</tbody>
</table>
<br><br>
<a style="text-decoration:none;font-size:18px;" href="{{ url('discussion/') }}"{!! Form::button('View Discussions' , ['id'=>'button']) !!}</a>{{--This button redirects the user to the page to view discussions--}}

            
            </div>
        </div>
    </body>
</html>
