<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
			$table->string('title', 50);
			$table->text('post');
			//table->integer('user_id')->unsigned();
			//$table->bigInteger('user_id);
			$table->unsignedBigInteger('user_id');
			//define the foreign keys
			$table->foreign('user_id')->references('id')->on('users');
            $table->text('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
