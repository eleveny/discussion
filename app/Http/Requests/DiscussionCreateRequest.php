<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscussionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
				'title' => "required|regex:^[a-zA-Z][0-9a-zA-Z]$^",
                'description' => "required|regex:^[a-zA-Z][0-9a-zA-Z]$^",
                'post'=> "required|regex:^[a-zA-Z]$^",
            //
			/*
			[a-zA-Z][0-9a-zA-Z]{5,10} lower and upper range
			[a-zA-Z][0-9a-zA-Z]{5,} No upper rangee
			[a-zA-Z0-9{6} a11111
			[a-zA-Z][0-9a-zA-Z]* EMPTY A aa
			[a-zA-Z][0-9a-zA-Z]+ a aa aaa aaaa
			*/
        ];
    }
}
