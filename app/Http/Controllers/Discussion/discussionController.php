<?php

namespace App\Http\Controllers\Discussion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Discussion;
use App\Http\Requests\DiscussionCreateRequest;

class discussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$discussions = Discussion::all();
		return view('Discussion/index')->with('discussions', $discussions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$discussion = new Discussion;
		return view('Discussion/create')->with('discussion', $discussion);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscussionCreateRequest $request)
    {
		Discussion::create([
			'user_id' => '1',
			'title' => $request->title,
            'description' => $request->description,
			'post' => $request->post,
		]);
		
		return redirect(url('discussion'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $discussion = Discussion::findOrFail($id);
        return view('Discussion/view')->with('discussion', $discussion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $discussion = Discussion::findOrFail($id);
        return view('Discussion/edit')->with('discussion', $discussion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DiscussionCreateRequest $request, $id)
    {
        //
        $discussion = Discussion::findOrFail($id);
        $discussion->title = $request->title;
        $discussion->description = $request->description;
        $discussion->post = $request->post;
        $discussion->save();
        return redirect(url('discussion'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$discussion = Discussion::findOrFail($id);
		$discussion->delete();
		return redirect(url('discussion'));
    }
}
