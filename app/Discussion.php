<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    //
	protected $fillable = [
		'user_id',
        'title',
        'description',
		'post',
	];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
    
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
}
