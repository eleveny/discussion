<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'user_id',
        'parent_id',
        'title',
        'description',
        'post',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
